import sys
import termios
import tty
import subprocess
import re
import time
import os
import argparse


def install_udev():
    ans = input('Do you want me to install the udev rule [Y]/N? ').upper()
    if 'N' in ans:
        return

    path = '/etc/udev/rules.d/99-spotlight.rules'
    rule = 'KERNEL=="hidraw*", SUBSYSTEM=="hidraw", MODE="0664", GROUP="%s"'
    rule = rule % (
        os.environ['SUDO_USER']
    )
    if os.path.isfile(path):
        raise FileExistsError('File %s exists, aborting' % path)
        return
    else:
        print('Installing the following rule to %s' % path)
        print(rule)

        with open(path, 'w') as fp:
            fp.write(rule)

        print('Reloading udev')
        if subprocess.Popen(['udevadm', 'control', '--reload-rules']).wait():
            raise IOError
        if subprocess.Popen(['udevadm', 'trigger']).wait():
            raise IOERror


def mkcursor(size=32, colour=[1, 0, 0, 1], solid=False):
    if 'SUDO_USER' in os.environ:
        import sys
        sys.path.append(
            '/home/%s/.local/lib/python%d.%d/site-packages' % (
                os.environ['SUDO_USER'],
                sys.version_info.major,
                sys.version_info.minor
            )
        )

    import numpy as np
    from PIL import Image
    img = np.zeros((size, size, 4))
    x, y = np.meshgrid(np.arange(size), np.arange(size))
    r2 = (x - size / 2)**2 + (y - size / 2)**2

    if solid:
        img[r2 < (size / 2)**2, :] = colour
    else:
        # we want the sigma such that at the maximal distance, i.e.
        # rmax=size/2,
        #     exp(-rmax^2/(2sigma^2)) = 1/255
        # Hence,
        #     rmax^2/ 2 / log(255) = sigma^2
        #     rmax / Sqrt[2 log(255)] = sigma
        mival = 1/255.
        sigma = size/2 / np.sqrt(2*np.log(1/mival))
        img[:, :, 0] = colour[0] * np.exp(-r2 / 2. / sigma**2)
        img[:, :, 1] = colour[1] * np.exp(-r2 / 2. / sigma**2)
        img[:, :, 2] = colour[2] * np.exp(-r2 / 2. / sigma**2)
        img[:, :, 3] = colour[3] * np.exp(-r2 / 2. / sigma**2)
    Image.fromarray(np.uint8(255*img)).save('cursor.png')

    names = [
        'arrow', 'based_arrow_up', 'bd_double_arrow', 'bottom_left_corner',
        'bottom_right_corner', 'bottom_side', 'circle', 'copy',
        'crossed_circle', 'crosshair', 'double_arrow', 'draft_large',
        'draft_small', 'fd_double_arrow', 'fleur', 'h_double_arrow', 'hand',
        'hand1', 'hand2', 'left_ptr', 'left_ptr_watch', 'left_side', 'link',
        'question_arrow', 'right_ptr', 'right_side', 'sb_down_arrow',
        'sb_h_double_arrow', 'sb_left_arrow', 'sb_right_arrow', 'sb_up_arrow',
        'sb_v_double_arrow', 'top_left_arrow', 'top_left_corner',
        'top_right_corner', 'top_side', 'v_double_arrow', 'xterm', 'watch',
        'X_cursor', '00008160000006810000408080010102',
        '028006030e0e7ebffc7f7070c0600140', '03b6e0fcb3499374a867c041f52298f0',
        '08e8e1c95fe2fc01f976f1e063a24ccd', '14fef782d02440884392942c11205230',
        '2870a09082c103050810ffdffffe0204', '3ecb610c1bf2410f44200f48c40d3599',
        '4498f0e0c1937ffe01fd06f973665830', '6407b0e94181790501fd1e167b474872',
        '640fb0e74195791501fd1ed57b41487f', '9d800788f1b08800ae810202380a0822',
        'c7088f0f3e6c8088236ef8e1e3e70000', 'd9ce0ab605698f320427677b458ad60b',
        'e29285e634086352946a0e7090d73106', 'fcf1c3c7cd4491d801f1e1c78f100000'
    ]
    dir = '/usr/share/icons/pylight/'
    try:
        os.makedirs(dir + 'cursors')
    except OSError:
        pass
    with open(dir + 'index.theme', 'w') as fp:
        fp.write('[Icon Theme]\n')
        fp.write('Name=pyLight\n')
        fp.write('Comment=pylight theme\n')
    with open(dir + 'cursor.theme', 'w') as fp:
        fp.write('[Icon Theme]\n')
        fp.write('Inherits=pyLight\n')

    with open('cursor.conf', 'w') as fp:
        fp.write('%d 0 0 cursor.png' % size)

    subprocess.Popen(['xcursorgen', 'cursor.conf', dir + 'cursors/base'])
    for i in names:
        os.symlink(dir + 'cursors/base', dir + 'cursors/' + i)


def winbounds():
    proc = subprocess.Popen('xwininfo', stdout=subprocess.PIPE)
    stdout = proc.communicate()[0].decode()
    x0, y0 = map(int, re.findall(r'Absolute upper-left [XY]: *([-\d]*)', stdout))
    w, h = map(int, re.findall(r'(?:Height|Width): *(\d*)', stdout))
    windowId = re.findall(r'Window id: (0x[\da-f]+)', stdout)[0]
    return [x0, y0, x0+w, y0+h], windowId


def focus(wid):
    subprocess.Popen(['wmctrl', '-i', '-a', wid])


def mouse():
    proc = subprocess.Popen([
        'xdotool', 'getmouselocation'
    ], stdout=subprocess.PIPE)
    stdout = proc.communicate()[0].decode()
    return map(int, re.findall(r'x:(\d*) y:(\d*)', stdout)[0])


def putmouseback(bounds):
    x, y = mouse()
    if (bounds[0] < x < bounds[2]) and (bounds[1] < y < bounds[3]):
        return
    if bounds[0] > x:
        x = bounds[0] + 1
    elif bounds[2] < x:
        x = bounds[2] - 1
    if bounds[1] > y:
        y = bounds[1] + 1
    elif bounds[3] < y:
        y = bounds[3] - 1
    subprocess.Popen(['xdotool', 'mousemove', str(x), str(y)])


def findhid():
    c = 0
    for d in sorted(os.listdir('/sys/class/hidraw/')):
        dev = os.path.basename(os.readlink('/sys/class/hidraw/%s/device' % d))
        if '046D:C53E' in dev:
            c += 1
            if c == 3:
                return d


def vibrate(intensity=0x80):
    msg = bytes([
        0x10, 0x01, 0x09, 0x1a, 0x03, 0xe8, intensity
    ])
    with open('/dev/' + findhid(), 'wb') as fp:
        fp.write(msg)


def setcursor(c):
    proc = subprocess.Popen([
        'gsettings', 'set', 'org.gnome.desktop.interface', 'cursor-theme', c
    ], stdout=subprocess.PIPE)


def getcursor():
    proc = subprocess.Popen([
        'gsettings', 'get', 'org.gnome.desktop.interface', 'cursor-theme'
    ], stdout=subprocess.PIPE)
    return proc.communicate()[0].strip()


def timer(t):
    nh = 20
    nv = 20

    def segment(ind):
        a, b, c, d, e, f, g = [
            int(x)
            for x in '{:0{size}b}'.format(ind, size=7)
        ]
        txt = [' ' * nh] * nv
        if a: txt[   0 ] = '#' * nh
        if g: txt[nv//2] = '#' * nh
        if d: txt[ -1  ] = '#' * nh

        if f: txt[:nv//2] = ['#'+i[1:] for i in txt[:nv//2]]
        if e: txt[nv//2:] = ['#'+i[1:] for i in txt[nv//2:]]
        if b: txt[:nv//2] = [i[:-1]+'#' for i in txt[:nv//2]]
        if c: txt[nv//2:] = [i[:-1]+'#' for i in txt[nv//2:]]
        return txt

    colon = [' ' * nh] * nv
    colon[nv//3] = ' '*(nh//2) + '#' + ' '*(nh//2-1)
    colon[2*nv//3] = ' '*(nh//2) + '#' + ' '*(nh//2-1)

    numbers = [
        # 0     1    2     3      4     5    6     7     8     9
        0x7e, 0x30, 0x6d, 0x79, 0x33, 0x5b, 0x5f, 0x70, 0x7f, 0x7b,
        # a    b      c     d     e     f
        0x77, 0x1f, 0x4e, 0x3d, 0x4f, 0x47
    ]

    mi = t // 60
    se = t % 60

    display = [
        segment(numbers[mi // 10]), segment(numbers[mi % 10]),
        colon,
        segment(numbers[se // 10]), segment(numbers[se % 10])
    ]

    hspacing = ' ' * max(nh//2, 2)
    display = '\n'.join(hspacing.join(i) for i in zip(*display))
    print('\033[2J\033[1;1H' + display)


class PyLight:
    def __init__(self, vibrations=[], showtime=False):
        self.vibrations = vibrations
        self.showtime = showtime
        self.t = 0.01

        self.t0 = None
        self.proc = None
        self.oldcursor = None

    def unclutter(self):
        self.proc = subprocess.Popen(['unclutter', '-idle', '0.1'])

    def find_win(self):
        self.bounds, wid = winbounds()
        self.wait()
        focus(wid)

    def wait(self):
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)

    def chcursor(self):
        self.oldcursor = getcursor()
        setcursor('pylight')

    def loop(self):
        if not self.t0:
            self.t0 = time.time()

        time.sleep(self.t)
        putmouseback(self.bounds)

        if self.showtime:
            timer(int(time.time() - self.t0))

        if len(self.vibrations) > 0:
            if self.vibrations[0][0] < time.time() - self.t0:
                vibrate(self.vibrations[0][1])
                self.vibrations.pop(0)

    def end(self):
        if self.oldcursor:
            setcursor(self.oldcursor)
        if self.proc:
            self.proc.kill()

    def main(self):
        self.unclutter()
        self.find_win()
        self.chcursor()
        try:
            while True:
                self.loop()
        except KeyboardInterrupt:
            pass
        finally:
            self.end()


def pair(arg):
    arg = arg.split(',')
    if len(arg) == 1:
        ts = arg[0]
        intensity = 0x80
    elif len(arg) == 2:
        ts = arg[0]
        intensity = int(arg[1], 0)
    else:
        raise argparse.ArgumentError

    ts = ts.split(':')
    if len(ts) == 1:
        time = int(ts[0])
    elif len(ts) == 2:
        time = 60 * int(ts[0]) + int(ts[1])
    elif len(ts) == 3:
        time = 3600 * int(ts[0]) + 60 * int(ts[1]) + int(ts[2])
    else:
        raise argparse.ArgumentError
    return [time, intensity]


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--install', action='store_true')
    parser.add_argument(
        '-a',
        action='append',
        type=pair,
        help='((hh:)mm:)ss(,intensity) at which to vibrate'
    )
    parser.add_argument(
        '-d',
        action='store_true',
        help='Display ASCII art timer'
    )
    parsed = parser.parse_args()

    if parsed.install:
        install_udev()
        mkcursor()
    else:
        PyLight(
            vibrations=parsed.a if parsed.a else [],
            showtime=parsed.d
        ).main()
