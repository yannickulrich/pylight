# pyLight

pyLight is a minimal driver for the [Logitech Spotlight](https://www.logitech.com/en-gb/products/presenters/spotlight-presentation-remote.html)
under Gnome. If you want a more complete driver with a graphical
interface, try [Projecteur](https://github.com/jahnf/Projecteur/).

## Features

 * Laser pointer
 * Trapping the pointer in the presentation window
 * Navigation keys
 * Vibration
 * Stop watch

## Installing pyLight

### Debian
To use pyLight, clone this repository and install
```bash
$ sudo apt install unclutter wmctrl xdotool
$ sudo python pylight.py --install
$ sudo cp 99-spotlight.rules /etc/udev/rules.d/  # (recommended)
```
It is recommended that you copy the udev rules to allow pyLight to
vibrate your Spotlight without using `sudo`.

*Note*: `unclutter` adds itself to the `Xsession`. To undo this,
```bash
$ sudo rm /etc/X11/Xsession.d/90unclutter
```

### Fedora
To build pyLight you need `numpy` and `pillow` installed. If you
haven't already,
```bash
$ pip install numpy pillow
```
To use pyLight, clone this repository and install
```bash
$ sudo dnf install xcursorgen xdotool wmctrl xwininfo libev-devel

# Build unclutter
$ git clone https://github.com/Airblader/unclutter-xfixes
$ sudo cp 99-spotlight.rules /etc/udev/rules.d/  # (recommended)
$ make -C unclutter-xfixes/ unclutter
$ mv unclutter-xfixes/unclutter ~/.local/bin

# Build pylight
$ sudo python pylight --install

# Clean up
$ sudo dnf remove xcursorgen libev-devel
```


## Usage
1. Connect your Spotlight through Bluetooth or USB. Note that
   vibration alerts are *not* supported over bluetooth)
2. start pyLight
```bash
$ python pylight.py
```
3. Click on the window containing your *already fullscreen*
   presentation (probably evince).
4. You now have a laser pointer.

**WARNING**: pyLight traps your mouse in the presentation window!

5. To escape, `ALT-TAB` back to the terminal and terminate pyLight
   with `^C`.

### Vibration alert
The Spotlight receive send vibration alerts from pyLight. You can
schedule as many alerts as you want with the `-a` flag:
```bash
$ python pylight.py -a 00:15:00 \
                    -a 00:17:00 \
                    -a 00:19:30
```
This schedules alerts at minutes 15, 17 and 19 the talk. You can
further specify the intensity of the vibration on a scale of `0x00` to
`0x80` (default)
```bash
$ python pylight.py -a 00:15:00,0x50 \
                    -a 00:17:00,0x50 \
                    -a 00:19:30,0x80
```
The timer starts as soon as you click on your talk.

### Timer
pyLight can present an ASCII art clock in the terminal in which you
run it. For this, the terminal needs to be at least 141x22 characters
in size.
```bash
$ python pylight.py -d
```
The timer starts as soon as you click on your talk.


## Behind the scenes
Slide navigation and cursor movements work without any modifications
on any reasonably modern Linux system.

pyLight creates a custom cursor that in the form of a red laser
pointer during installation. When running pyLight, this cursor is
activated using `gsettings`. When the pointer is turned off, it is
hidden using [`unclutter`](https://wiki.archlinux.org/title/unclutter).

The cursor is trapped in a pre-calculated area on the screen using
`xdotool` as long as pyLight is running. This is to avoid accidentally
losing the cursor on another screen, if a joined display mode is used.

### Vibration
*Vibration alerts have been adapted from [the research for Projecteur](https://github.com/jahnf/Projecteur/issues/6).*

To vibrate the Spotlight, the following seven bytes need to be send to
the raw HID device
```
0x10
0x01
0x09
0x1a
0x03   # length between 0x00 and 0x0a
0xe8
0x80   # intensity between 0x00 and 0x80
```
This gets written to `/dev/hidraw<n>` where `n` is the device number
assigned by the kernel. You can find this by grepping for the USB ID
```bash
$ ls -l /sys/class/hidraw/*/device | grep 046D:C53E
```
To execute a vibration, simply run
```bash
$ echo -en '\x10\x01\x09\x1a\x03\xe8\x80' > /dev/hidraw<n>
```
